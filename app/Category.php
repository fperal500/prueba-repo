<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{   

    // Nombre de la tabla en MySQL.
	protected $table='ps_category';

    protected $dates=['date_add','date_upd'];

    protected $fillable =[
        'id_category',
        'id_parent',
        'level_depth',
        'nleft',
        'nright',
        'active',
        'position',
        'is_root_category',
        'name'
    ];

    public function products(){
        return $this->hasMany(Product::class,'id_category');
        
    }

}

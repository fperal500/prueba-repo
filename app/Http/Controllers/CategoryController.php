<?php
namespace App\Http\Controllers;
use App\Category;
use App\Product;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
 use App\Http\Resources\CategoryCollection;
 use App\Http\Resources\CategoryResource;
use Unlu\Laravel\Api\QueryBuilder;

class CategoryController extends Controller
{
    
    public function index(Request $request)
    {   
        
       
        
        $data = DB::table('ps_category')
        ->join('ps_product', 'ps_category.id_category', '=', 'ps_product.id_product')
        ->join('ps_category_product','ps_category.id_category','=','ps_category_category_product.id_category')
        ->select('id_category', 'ps_category.name','ps_product.id_product','ps_product.name','ps_parent')
        
        ->get();    
        return $data; 
        
    }



    public function show(Category $category)
    {
        return $category;

    }


  
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

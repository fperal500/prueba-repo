<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        return[

        'id_product' => $this->id_product,
        'name' => $this->name,
        'price' => $this->price,
        'id_category_default' => $this->id_category_default,
         'category' => new CategoryResource($this->category),
        ];
        
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="ps_product";

    protected $date=['date_add','date_upd'];
   
    protected $fillable=[
        'id_product',
        'name',
        'price',
        'quantity',
    ];

    public function category(){
        return $this->belongsToMany('App\Category','id_category')->withPivot('id_category','id_product');
    }
}